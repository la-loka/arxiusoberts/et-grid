(function($){
	$.fn.tablesorter = function() {
		var $table = this;
		this.find('th').click(function() {
			var idx = $(this).index();
			var direction = $(this).hasClass('sort_asc');
			$table.tablesortby(idx,direction);
		});
		return this;
	};
	$.fn.tablesortby = function(idx,direction) {
		var $rows = this.find('tbody tr');
		function elementToVal(a) {
			var $a_elem = $(a).find('td:nth-child('+(idx+1)+')');
			var a_val = $a_elem.attr('data-sort') || $a_elem.text();
			return (a_val == parseInt(a_val) ? parseInt(a_val) : a_val);
		}
		$rows.sort(function(a,b){
			var a_val = elementToVal(a), b_val = elementToVal(b);
			return (a_val > b_val ? 1 : (a_val == b_val ? 0 : -1)) * (direction ? 1 : -1);
		})
		this.find('th').removeClass('sort_asc sort_desc');
		$(this).find('thead th:nth-child('+(idx+1)+')').addClass(direction ? 'sort_desc' : 'sort_asc');
		for(var i =0;i<$rows.length;i++)
			this.append($rows[i]);
		this.settablesortmarkers();
		return this;
	}
	$.fn.retablesort = function() {
		var $e = this.find('thead th.sort_asc, thead th.sort_desc');
		if($e.length)
			this.tablesortby($e.index(), $e.hasClass('sort_desc') );
		
		return this;
	}
	$.fn.settablesortmarkers = function() {
		this.find('thead th span.indicator').remove();
		this.find('thead th.sort_asc').append('<span class="indicator">&darr;<span>');
		this.find('thead th.sort_desc').append('<span class="indicator">&uarr;<span>');
		return this;
	}
})(jQuery);

function ETGrid(options) {
    // properties
    var cache = new Array();
    var dictionary = {
        "en": {
            "Start": "Start",
            "Search": "Search",
            "Name": "Name",
            "Size": "Size",
            "Date": "Date",
            "items": "items",
            "Load more results": "Load more results",
            "Search documents by key words": "Search documents by key words",
            "Search results": "Search results"
        },
        "es": {
            "Start": "Inicio",
            "Search": "Buscar",
            "Name": "Nombre",
            "Size": "Tamaño",
            "Date": "Fecha",
            "items": "ítems",
            "Load more results": "Cargar más resultados",
            "Search documents by key words": "Busca documentos por palabras clave",
            "Search results": "Resultados de la búsqueda"
        },
        "ca": {
            "Start": "Inici",
            "Search": "Cerca",
            "Name": "Nom",
            "Size": "Mida",
            "Date": "Data",
            "items": "ítems",
            "Load more results": "Carrega més resultats",
            "Search documents by key words": "Cerca documents per paraules clau",
            "Search results": "Resultats de la cerca"
        },
    };
    var resource = options.resource;
    var currentPage = 0;
    if (typeof options.language != "undefined") var language = options.language; else var language = 'es';
    if (typeof options.crossdomain != "undefined"){
        if (options.crossdomain == "true") var datatype = "jsonp"; else var datatype = "json";
    }else{
            var datatype = 'jsonp';
    }
    if (typeof options.search_button_icon != "undefined") var $search_button_icon = $(options.search_button_icon);
    if (typeof options.search_loader_icon != "undefined") var $loader_glyph = $(options.search_loader_icon); else var $loader_glyph = $('<div>Loading ...</div>');
    if (typeof options.folder_icon != "undefined") var $folder_icon = $(options.folder_icon); else var $folder_icon = $('<span></span>');
    if (typeof options.file_icon != "undefined") var $file_icon = $(options.file_icon); else var $file_icon = $('<span></span>');

    $loader_glyph.addClass("ET-loader-glyph");
    $search_loader_icon = $loader_glyph.clone();
    $search_loader_icon.attr("id", "ETGrid-search-form-loader");
    elements = setUpTable();
    var $table = elements[0];
    var $tbody = elements[1];
    var $folder_description = $('#ETGrid-description');
    var $search_form = setupSearchForm();
    $table.tablesorter();
    var $crumbs = $('#ETGrid-crumbs');
    setupBreadcrumbs();
    
    /*
     * Event bindings
     */
    $(window).on('hashchange', function(event){
        currentPage=0;
        var hashval = window.location.hash.substr(1);
        if (hashval.substring(2, 0) == "q="){
            var search_text = decodeURI(hashval.substring(2));
            $search_form.find("input").val(search_text)
            search(search_text);
        }else{
            $search_form.find("input").val("");
            listFiles();
        }
    }).trigger('hashchange');

    $crumbs.on('click', '#ET-grid-firstcrumb', function(event){
        event.preventDefault();
        currentPage=0;
        $search_form.find("input").val("");
        history.pushState("", document.title, window.location.pathname);
        listFiles();
    });

    $search_form.on('submit', function(event){
        event.preventDefault();
        var search_text = $search_form.find("input").val();
        if (search_text != ""){
            window.location.assign('#q='+search_text);
        }else{
            currentPage=0;
            history.pushState("", document.title, window.location.pathname);
            listFiles();
        }
    });

    $('#ETGrid-nextPageOption').on('click', '#ETGrid-nextPageOption-button', function(event){
        $('#ETGrid-nextPageOption-button').replaceWith($loader_glyph)
        search($search_form.find("input").val());
    });

    /*
     * Ajax calls
     */
    function listFiles(){
        var hashval = window.location.hash.substr(1);
        cacheIndex=hashval;
        if (cacheIndex==""){
            cacheIndex="cache_index_for_root_directory";
        }
        if (typeof cache[cacheIndex] != "undefined"){
            renderGrid(cache[cacheIndex]);
            renderBreadcrumbs(hashval);
            return;
        }
        $.ajax({
            url: resource,
            data: {'do':'list', 'file':hashval },
            dataType: datatype,
            beforeSend: function() { },
            complete: function() { $('.ET-Grid').fadeIn(100); },
            success: function(data){
                cache[cacheIndex]=data;
                renderGrid(data);
                renderBreadcrumbs(hashval);
            },
            error: function(data){console.log(data)}
        });
    }
    function search(search_text){
        var cacheIndex = JSON.stringify(search_text+currentPage);
        if (search_text==""){
            cacheIndex="cache_index_for_root_directory";
        }
        if (typeof cache[cacheIndex] != "undefined"){
            renderGrid(cache[cacheIndex]);
            renderBreadcrumbs(dictionary[language]['Search results']);
            return;
        }
        $.ajax({
            url: resource,
            data: {'do':'search', 'search_text':search_text, 'currentPage':currentPage},
            dataType: datatype,
            beforeSend: function() { $('#ETGrid-search-form-loader').css('display', 'inline-block'); },
            complete: function() { $('.ET-Grid').fadeIn(100); },
            success: function(data){
                renderGrid(data);
                renderBreadcrumbs(dictionary[language]['Search results']);
                cache[cacheIndex]=data;
            },
            error: function(data){console.log(data)}
        });     
    }
    /*
     * Render results
     */
    function renderGrid(data){
        $folder_description.html(data.folder_description);
        if (currentPage==0){ // we are rendering page 0
            emptyGrid();
        }
        $.each(data.results,function(k,v){
            $tbody.append(renderFileRow(v));
        });
        if(data.nextPage){
            showNextPageOption()
        }else{
            totalItems=currentPage+data.results.length;
            $table.find('tfoot').remove();
            $table.append('<tfoot id="ET-grid-table-tfoot"><tr><td class="empty" colspan=3>'+totalItems+' '+dictionary[language]['items']+'</td></tr></tfoot>');
        }
        currentPage=data.nextPage;
        $table.retablesort();
        $('.ET-loader-glyph').hide();
    }
	function renderBreadcrumbs(path) {
        $list=$('<ol></ol>');
        $list.append('<li><a id="ET-grid-firstcrumb" href=#>'+dictionary[language]['Start']+'</a></li>');
        var base = "";
        arr = path.split('/');
		$.each(arr, function(k,v){
			if(v) {
                v = decodeURI(v);
                if (k == arr.length-1){ // the last crumb in the list
                    $list.append( $('<li/>').addClass('active').text(v) );
                }else{
                    $item = $('<li/>').append( $('<a/>').attr('href','#'+base+v).text(v) );
                    $list.append($item);
                    base += v + '/';
                }
			}
		});
        $crumbs.html($list);
	}
	function renderFileRow(data) {
        if (data.is_dir){
            var $icon = $folder_icon.clone();
        }else{
            var $icon = $file_icon.clone();
        }
		var $link = $('<a class="name" />')
			.attr('href', data.is_dir ? '#' + data.path : data.url)
            .attr('target', data.is_dir ? '' : '_blank')
			.text(data.name);
		var $html = $('<tr />')
			.addClass(data.is_dir ? 'is_dir' : '')
			.append( $('<td class="first" />').append($icon).append($link) )
			.append( $('<td/>').attr('data-sort',data.is_dir ? -1 : data.size)
            .html($('<span class="size" />').text(formatFileSize(data.size))) ) 
			.append( $('<td/>').attr('data-sort',data.mtime).text(formatTimestamp(data.mtime)) )
		return $html;
	}
    function emptyGrid(){
        $tbody.empty();
        $('#ET-grid-table-tfoot').remove();
        emptyNextPageOption();
    }
    function showNextPageOption(){
        $('#ETGrid-nextPageOption').html('<button id="ETGrid-nextPageOption-button" type="button">'+dictionary[language]['Load more results']+'</button>');
    }
    function emptyNextPageOption(){
        $('#ETGrid-nextPageOption').empty();
    }
    
    /*
     * Set up layout
     */
    function setUpTable(){
        var $table = $('<table class="ET-filegrid-table"></table>');
        $table.append('<thead><tr><th><span>'+dictionary[language]['Name']+'</span></th><th><span>'+dictionary[language]['Size']+'</span></th><th><span>'+dictionary[language]['Date']+'</span></th></tr></thead>');
        var $tbody=$('<tbody></tbody>');
        $table.append($tbody);
        $('#ETGrid-files').append($table);
        $table.after('<div id="ETGrid-nextPageOption"></div>');
        return [$table, $tbody];
    }
    function setupSearchForm(){
        var $searchForm = $('<form id="ET-search-form"></form>');
        var $searchForm_inputGroup = $('<div class="ET-search-form-inputGroup"></div>');
        $searchForm_inputGroup.append($search_loader_icon);
        $searchForm_inputGroup.append('<input class="ET-search-form-input" type="text" placeholder="'+dictionary[language]['Search documents by key words']+'">');
        var $searchForm_buttonGroup = $('<span class="ET-search-form-buttonGroup"></span>');
        var $searchForm_button = $('<button class="ET-search-form-button" type="button" onclick="$(\'#ET-search-form\').submit()"></button>');
        if (typeof $search_button_icon != "undefined"){
            $searchForm_button.append($search_button_icon);
        }else{
            $searchForm_button.text(dictionary[language]['Search']);
        }
        $searchForm_buttonGroup.append($searchForm_button);
        $searchForm_inputGroup.append($searchForm_buttonGroup);
        $searchForm.append($searchForm_inputGroup);
        $('#ETGrid-search').append($searchForm);
        return $searchForm;
    }
	function setupBreadcrumbs(){
        $list=$('<ol></ol>');
        $list.append('<li class="active">'+dictionary[language]['Start']+'</li>');
        $crumbs.html($list);
	}
    /*
     * Formaters
     */
	function formatTimestamp(unix_timestamp) {
		var d = new Date(unix_timestamp*1000);
        var month = d.getMonth()+1;
        return d.getDate()+'/'+month+'/'+d.getFullYear();
	}
	function formatFileSize(bytes) {
		var s = ['bytes', 'KB','MB','GB','TB','PB','EB'];
		for(var pos = 0;bytes >= 1000; pos++,bytes /= 1024);
		var d = Math.round(bytes*10);
		return pos ? [parseInt(d/10),".",d%10," ",s[pos]].join('') : bytes + ' bytes';
	}
}
